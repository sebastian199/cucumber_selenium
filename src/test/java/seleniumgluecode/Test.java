package seleniumgluecode;




import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Dado;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Actions;
import sun.java2d.Spans;
import java.util.List;
import java.util.concurrent.TimeUnit;





public class Test {

    private ChromeDriver driver = Hooks.getDriver();

    @Dado("^que la web Utest esta disponible$")
    public void que_la_web_Utest_esta_disponible() throws Throwable {
        WebElement titleJoin = driver.findElement(By.className("unauthenticated-nav-bar__sign-up"));
        titleJoin.click();
    }

    @Cuando("^registro mis datos y doy click$")
    public void registro_mis_datos_y_doy_click() throws Throwable {

        // Primer Formulario

        driver.findElement(By.id("firstName")).sendKeys("Sebastian");
        driver.findElement(By.id("lastName")).sendKeys("Hernan");
        driver.findElement(By.id("email")).sendKeys("jsjuans@gmail.com");
        Select fecha = new Select(driver.findElement(By.id("birthMonth")));
        fecha.selectByIndex(3);
        Select dia = new Select(driver.findElement(By.id("birthDay")));
        dia.selectByIndex(22);
        Select año = new Select(driver.findElement(By.id("birthYear")));
        año.selectByIndex(16);
        WebElement registrar = (driver.findElement(By.className("btn-blue")));
        registrar.click();

        //Segundo formulario  - Problemas de sellecion para pasar al 3 formulario

        driver.findElement(By.id("city")).sendKeys("Bogotá");
        driver.findElement(By.id("zip")).sendKeys("100101");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", driver.findElement(By.className("btn-blue")));

        //Tercer formulario

        Select OS = new Select(driver.findElement(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[1]/div[1]/div[2]/div/div[1]/span")));
        OS.selectByIndex(3);
        Select version = new Select(driver.findElement(By.xpath("//*[@id=\"web-device\"]/div[2]/div[2]/div/div[1]/span")));
        version.selectByIndex(8);
        Select lengu = new Select(driver.findElement(By.xpath("//*[@id=\"web-device\"]/div[3]/div[2]/div/div[1]/span")));
        lengu.selectByIndex(7);
        WebElement registrarr = (driver.findElement(By.className("btn-blue")));
        registrarr.click();

        //Cuarto formulario

        driver.findElement(By.id("passord")).sendKeys("Elseñor2021+");
        driver.findElement(By.id("confirmPassword")).sendKeys("Elseñor2021+");
        WebElement labela = (driver.findElement(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]")));
        labela.click();
        WebElement labelo = (driver.findElement(By.className("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]")));
        labelo.click();



        //Codigos para seleccionar el boton

    //span[@class="btn btn-default form-control ui-select-toggle"]
        //driver.findElement(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a")).click();
        //driver.findElement((By.cssSelector("a.btn-blue-Next: Devices")));
        //driver.findElement(By.cssSelector("button.dialog-confirm")).click()
        //river.findElement(By.xpath("//span[starts-with(Next: Devices)]")).click();
        //driver.findElement(By.xpath("//span[contains(Next: Devices)]"))
        //WebElement searchBtn = driver.findElement(By.partialLinkText("Next: Devices"));
        //Actions actionProvider = new Actions(driver);
        // Realiza la acción click-and-hold en el elemento
        //actionProvider.clickAndHold(searchBtn).build().perform();
        //driver.findElement(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a")).click();
        //Select ciudad = new Select(driver.findElement(By.id("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/div[1]/span")));
        //ciudad.selectByIndex(5);
        //WebElement reegistrar = (driver.findElement(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a")));
        //reegistrar.click();
      // by_xpath("//div[@id='mainmenu']/ul/li[5]")
        //*[@id="regs_container"]/div/div[2]/div/div[2]/div/form/div[2]/div/a
    }

    @Entonces("^Se muestra$")
    public void se_muestra() throws Throwable {

        WebElement registrarfinal = (driver.findElement(By.className("btn-blue")));
        registrarfinal.click();


    }



}
